﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.DefaultMenu" Type="Str">dir.mnu</Property>
	<Property Name="NI.Lib.Description" Type="Str">LabVIEW Plug and Play instrument driver for

&lt;fill in information about manufacturer, model, and type of the instrument&gt;.</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!**!!!*Q(C=\&gt;3^&lt;?.!%)&lt;BT]9&amp;F[I$1SV-#SLBW-)E3A]1=*=Y6!P4AAQH4AS9,5Q,CC]QL":U\S\(N#)JM1%&lt;].*,C&gt;`_06L3F(LZ+@X1]6*:HSWN8?W05TOL@5Z*$[@WON"LA_&lt;,XFZD4]?`&gt;JX(PQV_[TT00R0G^N0VT`NVNHSX@\&gt;`\@9,B4[X&amp;TN^=*'OOJ3;V+![V@KP/S:ZEC&gt;ZEC&gt;ZEA&gt;ZE!&gt;ZE!&gt;ZE$OZETOZETOZEROZE2OZE2OZE@?$8/1C&amp;TG\ECS?,*2-GES1&gt;);CZ*2Y%E`C34R=+P%EHM34?")08:2Y%E`C34S*BW&amp;+0)EH]33?R-.58:,^)-?4?*B?A3@Q"*\!%XB95I%H!!3,"2-(E]"1U"B]#4S"*`$Q69%H]!3?Q".Y;&amp;&lt;A#4S"*`!%(I&lt;U89GO;1=Z(K;2YX%]DM@R/"[GFO.R0)\(]4A?FJ0D=4Q/QFH1G2S#H%&amp;/"_@#]4A?0O2Y()`D=4S/B[:_B\TP4./UARS0Y4%]BM@Q'"[GE/%R0)&lt;(]"A?JJ8B-4S'R`!9(J;3Y4%]BM?!')OSP)T*D)&amp;'*S-Q00TVJ]8[89IOM8\5P_&lt;UI+I?1.7$J8JA6!_#[A;L&lt;JTKBKAW7L7"KIV2`7$6$V%"61OL*F2VV)(THDJ3&gt;^1N&gt;5-&gt;K#PKELJI1^_ZY_&amp;QU([`VTC/WOVWWG[XWGQW'I:"K^6+S_63C]6C@FP&gt;=%TF[P3^^0,\\`JZ?,D\^_P0Y`0^U^X,`&lt;BO_3X^0KJ/\[80]'\5N9YHLXHW[$`H`%DE!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Default Instrument Setup.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Private/Default Instrument Setup.vi"/>
		<Item Name="Default Instrument Wait.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Private/Default Instrument Wait.vi"/>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Action-Status" Type="Folder">
			<Item Name="Compensation" Type="Folder">
				<Item Name="Compensation.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Action-Status/Compensation/Compensation.mnu"/>
				<Item Name="External Compensation Status.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Action-Status/Compensation/External Compensation Status.vi"/>
			</Item>
			<Item Name="Trigger" Type="Folder">
				<Item Name="External Trigger Status.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Action-Status/Trigger/External Trigger Status.vi"/>
				<Item Name="Trigger.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Action-Status/Trigger/Trigger.mnu"/>
			</Item>
			<Item Name="Action-Status.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Action-Status/Action-Status.mnu"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Beep" Type="Folder">
				<Item Name="Beep.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Beep/Beep.mnu"/>
				<Item Name="Enable Beep.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Beep/Enable Beep.vi"/>
			</Item>
			<Item Name="Compensation" Type="Folder">
				<Item Name="Compensation.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Compensation/Compensation.mnu"/>
				<Item Name="Enable External Compensation.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Compensation/Enable External Compensation.vi"/>
			</Item>
			<Item Name="Lock" Type="Folder">
				<Item Name="Lock Buttons.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Lock/Lock Buttons.vi"/>
				<Item Name="Lock.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Lock/Lock.mnu"/>
			</Item>
			<Item Name="Slope" Type="Folder">
				<Item Name="Configure Voltage Slope.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Slope/Configure Voltage Slope.vi"/>
				<Item Name="Slope.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Slope/Slope.mnu"/>
			</Item>
			<Item Name="Trigger" Type="Folder">
				<Item Name="Enable External Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Trigger/Enable External Trigger.vi"/>
				<Item Name="Trigger.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Trigger/Trigger.mnu"/>
			</Item>
			<Item Name="Configure Current Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Configure Current Limit.vi"/>
			<Item Name="Configure Output.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Configure Output.vi"/>
			<Item Name="Configure Voltage Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Configure Voltage Limit.vi"/>
			<Item Name="Configure.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Configure.mnu"/>
			<Item Name="Enable Output.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Configure/Enable Output.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Memory" Type="Folder">
				<Item Name="Read" Type="Folder">
					<Item Name="Read.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Data/Memory/Read/Read.mnu"/>
					<Item Name="Recall Dynamic List.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Data/Memory/Read/Recall Dynamic List.vi"/>
					<Item Name="Recall From Memory.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Data/Memory/Read/Recall From Memory.vi"/>
				</Item>
				<Item Name="Write" Type="Folder">
					<Item Name="Save To Memory.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Data/Memory/Write/Save To Memory.vi"/>
					<Item Name="Write.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Data/Memory/Write/Write.mnu"/>
				</Item>
				<Item Name="Memory.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Data/Memory/Memory.mnu"/>
			</Item>
			<Item Name="Data.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Data/Data.mnu"/>
			<Item Name="Read Current Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Data/Read Current Limit.vi"/>
			<Item Name="Read Output.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Data/Read Output.vi"/>
			<Item Name="Read Voltage Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Data/Read Voltage Limit.vi"/>
			<Item Name="Read Voltage Slope.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Data/Read Voltage Slope.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Error Query.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Utility/Error Query.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Utility/Revision Query.vi"/>
			<Item Name="Self-Test.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Utility/Self-Test.vi"/>
			<Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Utility/Utility.mnu"/>
		</Item>
		<Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Close.vi"/>
		<Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/dir.mnu"/>
		<Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="/&lt;instrlib&gt;/RND 320-KWR10X/Public/VI Tree.vi"/>
	</Item>
	<Item Name="RND 320-KWR Series Readme.html" Type="Document" URL="/&lt;instrlib&gt;/RND 320-KWR10X/RND 320-KWR Series Readme.html"/>
</Library>
